---
title: 'リンク'
date: '2021-09-21'
author: 'Ken Wakita'
---

# リンク (Ken Wakita, 2021-09-21)

リンクの貼り方について悩んだ。いくつかの設定と Jekyll のバージョンが絡んだ問題だったようだ。

- `baseurl` と `url` の設定：なにかの例を見て `url` に GitHub の URL を設定していたが、それを空にした。
    ~~~
    baseurl: "/classes"
    url: ""       
    ~~~

- [Jekyll V4.0 以降ではリンクの設定が簡素化され](https://jekyllrb.com/docs/liquid/tags/#links)、`site.baseurl` の参照が不要になったとドキュメントに書かれている。ということは、それより古いバージョンの Jekyll を利用している場合はこれを明示すべきなのだろう。

    ~~~ {.md}
    ここにある[各クラスの第4クォーターの内容説明]({{site.baseurl}}{% link years/y21/cs2/course.md %})を読んで
    ~~~

    なぜ、手元の Jekyll のバージョンが 3.9.0 と古いのかは不明。
