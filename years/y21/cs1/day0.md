---
title: 'Day 0: ガイダンス'
---

{% include y21/cs1.md %}

---

![]({{ site.baseurl }}{% link assets/images/y21-cs1/slack.png %})

## 講義前日までの準備

T2SCHOLA でアナウンスした Slack の招待リンクを開いてこの科目の Slack に参加して下さい。授業の基本情報、各種資料、ビデオはすべて Slack で提供します。

**演習に Google Account が必要です。取得して下さい。**コンピュータサイエンス第一の演習では[Google Colaboratory](https://colab.research.google.com/)というGoogleのサービスを利用します。このために Google のアカウントが必要になります。すでに Gmail を持っている人、Google Drive をはじめとしたGoogoleのサービスを利用している人は、そのアカウント (Gmail のメールアドレス) を利用できます。

[Google のアカウント](https://www.google.com/account/about/)を持っていない人は用意しておいて下さい。[Google アカウント](https://www.google.com/account/about/)のページの下の方にある**アカウントを作成する**から作成できると思います。

初回の授業から Google Colaboratory の機能を使って実習をしますし、課題の実施にも必要になります。


## 授業当日

1. Slack にはいって下さい

1. Slack の `#general` チャネルに Zoom URL をブックマークしてあります。それをクリックして Zoom にはいって下さい。この操作がよくわからない人は、T2SCHOLA のアナウンスにも Zoom URL を書いたので、それを利用して Zoom に参加しても構いません。

1. Zoom では氏名で入室して下さい。ニックネームは使わないで下さい。

1. Zoom の授業の模様は録画・録音します。録画した授業のビデオは Slack を介して共有します。

