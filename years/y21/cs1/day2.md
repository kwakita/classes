---
title: 'Day 2: 「データ = 数」；コンピュータの中では？'
date: '2021-10-18'
---

{% include y21/cs1.md %}

---

# 今日の授業の概要

- データ = 数

- コンピュータの中では？

# レポート課題「四則演算でアニメーション」が出ています

# 目次

1. Day 2 のノートブックを開く（自分の Google Drive への保存を忘れないこと ← 忘れるとせっかくの作業内容を保存できませんよ）

1. データは数である

    数で「文字、画像、音、映像」を表現すること

1. コンピュータのなかは？

    - **メモリー**はデータ（＝数）を格納する箱

    - **演算装置**とは？

    - データのやり取り：**入出力**

1. 練習問題：乗算を±1と繰り返しだけで実現しよう

1. レポート課題1の説明（スマイルくんのバリエーション）
