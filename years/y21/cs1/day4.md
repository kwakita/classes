---
title: 'Day 4: プログラミング体験1: 配列、for文、文字列と文字'
date: '2021-11-8'
---

{% include y21/cs1.md %}

# 今日の内容

- 前回の講評

    2-1 max: 凝ったプログラムの紹介。最大値が番兵よりも小さい場合に対応した人、最大値が複数箇所に出現した場合に対応した人

    2-2 filter: 論理演算の意味、非論理値と if の解釈の解説。項の扱いについての解説。

- 例題：頻度分布作成

- レポート課題「循環小数を止めよ！」の解説と議論。

# レポート課題2: 「循環小数の循環を止めよ！」

（授業時間内に解説しきれなかったので）締切は予定よりも延長しました。Slack で確認して下さい。

# これまでに学んだ Python の演算子

算術演算子 | 使用例   | 意味
---------- | -------- | ---------------------------
`+`        | `x +  y` | `x`と`y`の加算
`-`        | `x -  y` | `x`と`y`の減算
`*`        | `x *  y` | `x`と`y`の乗算
`//`       | `x // y` | `x`を`y`で割った商
`%`        | `x %  y` | `x`を`y`で割った余り
`**`       | `x ** y` | `x`の`y`条


関係演算子 | 使用例   | 意味
---------- | -------- | ---------------------------
`<=`       | `x <= y` | `x`は`y`より小さいか等しい
`<`        | `x <  y` | `x`は`y`より小さい
`==`       | `x == y` | `x`は`y`と等しい
`!=`       | `x != y` | `x`は`y`と等しくない
`>`        | `x >  y` | `x`は`y`より大きい
`>=`       | `x >= y` | `x`は`y`より大きいか等しい

論理演算子 | 使用例    | 意味
---------- | --------- | ---------------------------
`and`      | `x and y` | `x`と`y`の論理積（両方が真のときだけ真）
`or`       | `x or y`  | `x`と`y`の論理和（両方が偽のときだけ偽；少なくとも一方が真のとき真）
`not`      | `not x`   | `x`の否定（`x`が真のとき偽、`x`が偽のとき真）

