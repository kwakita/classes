---
title: 'LX04: Data Abstraction'
date: '2021-12-14'
---

{% include y21/vis.md %}

# 今日の内容

- Tamara Munzner の教科書から "Data Abstraction" の解説

# 参考図書

- Chapter 2: Data Abstraction in Munzner, Tamara, "[Visualization: Analysis and Design](https://www.routledge.com/Visualization-Analysis-and-Design/Munzner/p/book/9781466508910)," AK Peters Visualization Series, 2015.
