---
title: 'Jupyter Lab 環境の構築方法'
date: '2021-12-21'
---

{% include y21/vis.md %}

---

# Python 関連の開発環境設定

Python を用いた開発では、仮想環境を作成して、そのなかで開発することが普通です。仮想環境はプロジェクトごとに作成します。以下では、研究プロジェクトの三つのラウンドに共通する仮想環境の構築方法を説明しています。

仮想環境は `vis21` と命名し、`~/.venvs/vis21` に設定してあります。Python の仮想環境は過去に複数のものがありました。`pyenv`, `virtualenv` などが知られています。これらは Python を拡張するシステムとして開発されましたが、Python 3.7 になって Python に標準的に備わった仮想システムが提供されるようになりました。これが `venv` です。以下では `venv` を用いた仮想環境を使った環境設定について説明します。


1. Python 3 をインストールすること。インストールされている Python のバージョンの確認は `python3 --version`。

    macOS の場合は `brew install python3`。Windows の場合は `python.org` からダウンロードしてインストール。

1. Python の仮想環境 (`vis21`) の作成

    - `mkdir ~/.venvs`
    - `python3 -m venv ~/.venvs/vis21`

1. 作成した仮想環境 (`vis21`) の起動

    `source ~/.venvs/vis21/bin/activate`

1. 仮想環境 (`vis21`) への各種パッケージのインストール

    `pip install --upgrade pip bokeh networkx jupyterlab pandas scikit-learn scipy`

    - `pip`: パッケージのインストーラ
    - `jupyterlab`: Jupyter Lab 統合開発環境
    - `scipy`: 基本科学技術計算（行列・ベクトル演算のための `numpy` も一緒にインストールされます）
    - `pandas`: （ご存知）データ分析
    - `networkx`: ネットワーク構造の表現と分析アルゴリズム
    - `scikit-learn`: 機械学習
    - `bokeh`: データ可視化


# Python の仮想環境の起動方法

`source ~/.venvs/vis21/bin/activate`

これを実行すると、シェルのプロンプトに `(vis21)` が表示されるようになります。


# Jupyter Lab の起動方法

Jupyter Lab はウェブブラウザ上に実装された Python の統合開発環境です。Python の仮想環境を起動したあとで、以下のコマンドを実行すれば Jupyter Lab が起動します。

`jupyter lab`

うまくいかない場合は以下を参考にして下さい。

- Jupyter Lab は一部のブラウザで動作しません。[サポートされているブラウザについての情報](https://jupyterlab.readthedocs.io/en/stable/getting_started/installation.html#supported-browsers)を参考にして下さい。

    Jupyter Lab がサポートしていないソフトウェアを標準ウェブブラウザに指定している場合には、そのブラウザが起動してしまいます。Jupyter Lab の画面が表示されたら、

    1. まず、起動してしまった標準ブラウザと Jupyter Lab のサーバとの接続を遮断して下さい。Jupyter Lab のメニューから **File** メニュー > **Log out** です。Jupyter Lab のノートブックに複数の接続はサポートされていないので、利用しない接続を遮断することは重要です。

    1. Jupyter Lab 起動時のログに、サーバへアクセスするための URL が出力されているはずです。この URL を Jupyter Lab がサポートしているウェブブラウザで開いて下さい。

        以下がアクセス情報の例です。ここに書かれている指示にしたがって URL をブラウザにペーストすれば開けると思います。

        ![](https://i.gyazo.com/3eb1f85f654cd75a58488325e8e51ad9.png)


# 他人からもらったノートブックを Jupyter Lab で開く前に

自分以外の人が作成した Jupyter Notebook をうまく実行できないことがあります。これは Jupyter のセキュリティ機構がノートブックの実行を制限しているためです。

インターネットからダウンロードされたり、他人から共有されたノートブックは、入手した時点では**信頼されていません**。信頼されていないノートブックを Jupyter Lab で開いて閲覧することはできますし、一部のコードの実行も可能です。しかし、OSに対する入出力は制限されているため、本来の動作と異なることがあります。

入手したノートブックの制限のない環境で実行したい場合には、以下のようにコマンドを用いて、そのノートブックを**信頼されたノートブック**に変換します。

~~~ sh
jupyter trust [ノートブック].ipynb
~~~

ここで`[ノートブック].ipynb`は信頼するノートブックのファイル名です。ノートブックを信頼する作業は、通常のコマンドラインからも、Jupyter Lab 内のターミナル環境からも実行可能です。


# Bokeh Documentation への導き

- [Plotting with basic glyphs](https://docs.bokeh.org/en/latest/docs/user_guide/plotting.html)
- [Providing data](https://docs.bokeh.org/en/latest/docs/user_guide/data.html)
- Making Interactions > [Adding widgets](https://docs.bokeh.org/en/latest/docs/user_guide/interaction/widgets.html)

- [Running a Bokeh server](https://docs.bokeh.org/en/latest/docs/user_guide/server.html)

    `bokeh` コマンドを使った本格的なアプリを作るには以下の Directory format が参考になります。Single module format は単純ですが、中途半端。
    - [Building Bokeh applications](https://docs.bokeh.org/en/latest/docs/user_guide/server.html#building-bokeh-applications)
        - [Directory format](https://docs.bokeh.org/en/latest/docs/user_guide/server.html#directory-format)
