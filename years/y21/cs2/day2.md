---
title: 'Day 2: アルゴリズム１'
date: '2021-12-13'
---

{% include y21/cs2.md %}

---

# 今日の授業の概要

1. 再帰関数とそのコンピュータのなかでの実現方法

1. 計算とアルゴリズムとプログラム、そして計算時間

1. ソーティング・ゲームで学ぶ「ソーティング」という問題
