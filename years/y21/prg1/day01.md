---
title: 'Day 1: 本当の Scala プロジェクト；本当の Scala 開発'
date: '2021-10-03'
---

{% include y21/prg1.md %}

# 今日の内容

- 本当の Scala プロジェクト - コンパイラの利用

- 本当の Scala 開発 - `sbt`

- GitHub での実習の進め方
