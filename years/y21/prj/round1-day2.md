---
title: 'day2'
date: '2021-10-19'
author: 'Ken Wakita'
---

# DAY2 (Ken Wakita, 2021-10-19)

## SGD (`sgd`)

## GapMinder (`gapminder`)

- Python3 で
    - `import bokeh`
    - `bokeh.sampledata.download()`
- （Python を抜けて）コマンド行から
    - `bokeh serve --show .`

