---
layout: page
title: posts
---

{% for post in site.posts %}
- {{ post.date }} - [{{ post.title }}]({{ site.baseurl }}/{{ post.url }})
{% endfor %}
