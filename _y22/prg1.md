---
title: 'プログラミング第一'
class_id: 'prg1'
---

{% include classinfo.md %}

TA（3名）
: 池守和槻 / 谷口力斗 / 古殿直也 いずれも増原研(M1)の猛者揃い

---
## 講義の内容


{% for page in site.pages %}
  {% if page.url contains "/years/y22/prg1/day" %}
- [{{ page.title }}]({{ site.baseurl }}{{ page.url }})
  {% endif %}
{% endfor %}

※ 演習の資料とビデオは別途配布します。T2SCHOLA か Slack を確認して下さい。
