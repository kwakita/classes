---
title: '研究プロジェクト'
class_id: 'prj'
---

{% include classinfo.md %}

- [第二ラウンド]({{ site.baseurl }}{% link years/y22/prj/round2.md %})

- [第三ラウンド]({{ site.baseurl }}{% link years/y22/prj/round3.md %})


---

# Python 関連の開発環境設定

Python を用いた開発では、仮想環境を作成して、そのなかで開発することが普通です。仮想環境はプロジェクトごとに作成します。以下では、研究プロジェクトの三つのラウンドに共通する仮想環境の構築方法を説明しています。

仮想環境は `labproject` と命名し、`~/.venvs/labproject` に設定してあります。Python の仮想環境は過去に複数のものがありました。`pyenv`, `virtualenv` などが知られています。これらは Python を拡張するシステムとして開発されましたが、Python 3.7 になって Python に標準的に備わった仮想システムが提供されるようになりました。これが `venv` です。以下では `venv` を用いた仮想環境を使った環境設定について説明します。


1. Python 3 をインストールすること。インストールされている Python のバージョンの確認は `python3 --version`。

    macOS の場合は `brew install python3`。Windows の場合は `python.org` からダウンロードしてインストール。

1. Python の仮想環境 (`labproject`) の作成

    - `mkdir ~/.venvs`
    - `python3 -m venv ~/.venvs/labproject`

1. 作成した仮想環境 (`labproject`) の起動

    `source ~/.venvs/labproject/bin/activate`

1. 仮想環境 (`labproject`) への各種パッケージのインストール

    `pip install --upgrade pip bokeh networkx pandas scikit-learn scipy`


# Python の仮想環境の起動方法

`source ~/.venvs/labproject/bin/activate`

これを実行すると、シェルのプロンプトに `(labproject)` が表示されるようになります。

# Bokeh Documentation への導き

- [Plotting with basic glyphs](https://docs.bokeh.org/en/latest/docs/user_guide/plotting.html)
- [Providing data](https://docs.bokeh.org/en/latest/docs/user_guide/data.html)
- Making Interactions > [Adding widgets](https://docs.bokeh.org/en/latest/docs/user_guide/interaction/widgets.html)

- [Running a Bokeh server](https://docs.bokeh.org/en/latest/docs/user_guide/server.html)

    `bokeh` コマンドを使った本格的なアプリを作るには以下の Directory format が参考になります。Single module format は単純ですが、中途半端。
    - [Building Bokeh applications](https://docs.bokeh.org/en/latest/docs/user_guide/server.html#building-bokeh-applications)
        - [Directory format](https://docs.bokeh.org/en/latest/docs/user_guide/server.html#directory-format)
