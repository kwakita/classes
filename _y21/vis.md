---
title: '情報可視化'
class_id: 'vis'
---

{% include classinfo.md %}

---
## 授業の内容

{% for page in site.pages %}
  {% if page.url contains "/years/y21/vis/lx" %}
- [{{ page.title }}]({{ site.baseurl }}{{ page.url }})
  {% endif %}
{% endfor %}
