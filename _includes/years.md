{% assign years = "2021, 2022" | split: ", " %}
{% for year in years -%}
  {% assign yr = year | remove_first: "20" -%}
[{{year}}年度]({{site.baseurl}}/years/y{{yr}}/)
  {% if forloop.last %}{% else %} / {% endif -%}
{% endfor %}
