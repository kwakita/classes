{% assign prg1 = site.data['classes']['2022']['prg1'] %}

{% for day in prg1 %}
  {% if day['内容'] == page.title %}

    {% if day['講義資料'] %}
- [講義資料]({{ day['講義資料'] }})
    {% endif %}

    {% if day['サンプルプログラム'] %}
- [サンプルプログラム]({{ day['サンプルプログラム'] }})
    {% endif %}

    {% if day['課題'] %}
- [課題]({{ day['課題'] }})
    {% endif %}

  {% endif %}
{% endfor %}
