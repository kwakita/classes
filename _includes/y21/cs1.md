{% assign cs1 = site.data['classes']['2021']['cs1'] %}

{% for day in cs1 %}
  {% if day['内容'] == page.title %}

    {% if day['講義資料'] %}
- [講義資料 (Google Colaboratory)]({{ day['講義資料'] }})
    {% endif %}

    {% if day['宿題'] %}
- [宿題 (Google Form)]({{ day['宿題'] }})
    {% endif %}

    {% if day['レポート課題'] %}
- [レポート課題 (Google Form)]({{ day['レポート課題'] }})
    {% endif %}

  {% endif %}
{% endfor %}
