{% assign course = site.data['classes']['2021']['vis'] %}

{% for day in course %}
  {% if page.title contains day['ID'] %}

    {% if day['講義資料'] %}
- [講義資料 (Google Colaboratory)]({{ day['講義資料'] }})
    {% endif %}

  {% endif %}
{% endfor %}
